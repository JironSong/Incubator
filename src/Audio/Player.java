package Audio;

import java.io.File;
import javax.sound.sampled.*;

/**
 * 背景音乐播放类
 * @author 宋子龙
 */
public class Player {
    public void playMusic(String musicLocation) {
        try {
            File musicPath = new File(musicLocation);
            if(musicPath.exists())
            {
                AudioInputStream audioInput = AudioSystem.getAudioInputStream(musicPath);
                Clip clip = AudioSystem.getClip();
                clip.open(audioInput);
                clip.start();
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
