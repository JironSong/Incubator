package Main;

import Audio.Player;
import View.OpenWindow;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main类是整个游戏的入口
 * @author 宋子龙
 */
public class Main extends Application {
    OpenWindow initWindow = OpenWindow.getInstance();
    @Override
    public void start(Stage primaryStage) throws Exception {
        initWindow.run(primaryStage);
    }

    public static void main(String[] args) {
        String filepath = "Sis puella magica!.wav"; //播放bgm
        Player musicObject = new Player();
        musicObject.playMusic(filepath);
        launch(args);
    }
}
