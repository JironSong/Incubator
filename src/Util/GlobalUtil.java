package Util;

import Dao.CourseDao;
import Dao.ScDao;
import Dao.StudentDao;
import Entity.Course;
import Entity.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * 全局的工具类，也是单例模式
 * @author 宋子龙，朱穆清
 */
public class GlobalUtil {
    private static GlobalUtil instance;
    private GlobalUtil() {
    }
    public static GlobalUtil getInstance() {
        if (instance == null) instance = new GlobalUtil();
        return instance;
    }
    /**学生的队列queue*/
    private static final ArrayList<Student> queue = new ArrayList<>();
    /**已“完成”的学生的收容finished*/
    private static final ArrayList<Student> finished = new ArrayList<>();
    public static ArrayList<Student> getFinished() { return finished; }

    //数据查询类
    private final StudentDao studentDao = StudentDao.getInstance();
    private final ScDao scDao = ScDao.getInstance();
    private final CourseDao courseDao = CourseDao.getInstance();
    /**悲叹之种的全局变量*/
    private int griefSeed = 0;
    public int getGriefSeed() { return griefSeed; }
    public void setGriefSeed(int griefSeed) { this.griefSeed = griefSeed; }

    public void init() {
        studentDao.delete();
        Student madoka = new Student("Madoka", 80, 1, 0.2);//数字先就这样，测试不好通关或者太容易通关再改
        Student homura = new Student("Homura", 50, 0.5, 0.2);
        Student kyouko = new Student("Kyouko", 50, 1, 0.1);
        Student sayaka = new Student("Sayaka", 50, 1.5, 0.2);
        Student mami = new Student("Mami", 30, 1, 0.2);
        studentDao.save(madoka);
        studentDao.save(homura);
        studentDao.save(kyouko);
        studentDao.save(sayaka);
        studentDao.save(mami);
        queue.add(homura);
        queue.add(madoka);
        queue.add(kyouko);
        queue.add(sayaka);
        queue.add(mami);
    }

    /**
     * @return 当前的学生
     */
    public Student findCurrentStudent() {
        return queue.get(0);
    }

    /**
     * 找到战斗的下一个学生
     * @return 下一个学生
     */
    public Student findNextStudent() {
        Student preJudged = queue.remove(0);
        if(preJudged.getIsLiving()==1 && preJudged.getIsGraduated()==0)
            queue.add(preJudged);
        else
            finished.add(preJudged);
        if(!queue.isEmpty())
            return queue.get(0);
        return null;
    }

    /**
     * 检查学生是否满足毕业条件(修过所有必修课，选修课选够4分)
     * @param student 要检查的学生
     * @return 布尔值，是否毕业
     */
    public boolean checkGraduate(Student student) {
        List<Course> learnt = scDao.getSelectedCourse(student.getStuName());
        List<Course> allCourses = courseDao.getAllCourse();
        /*System.out.println(student.getStuName()+"学过的课：");
        System.out.println(learnt);*/

        boolean isConcluded;
        for(Course course : allCourses) {
            isConcluded = false;
            for(Course course1 : learnt) {
                if(course.getCourseID() == course1.getCourseID()) {
                    isConcluded = true;
                    break;
                }
            }
            if(course.getIsCompulsory().equals("是") && !isConcluded) {
                return false;
            }
        }

        double electiveCredit = 0.0;
        for(Course course : learnt) {
            if(course.getIsCompulsory().equals("否"))
                electiveCredit += course.getCourseCredit();
        }
        // System.out.println(electiveCredit);
        return electiveCredit >= 4.0; //暂定要求选修课修够4学分
    }
}
