package Dao;

import Entity.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * SCDao，具有查询插入删除的功能
 * @author 宋子龙
 */
public class ScDao extends BaseDao {
    private static ScDao instance;
    private ScDao() {
    }
    public static ScDao getInstance() {
        if (instance == null) instance = new ScDao();
        return instance;
    }
    /**清空表*/
    public void delete() {
        this.executeSql("Delete from sc");
    }
    /**插入记录*/
    public void save(int studentID, int courseID) {
        this.executeSql("insert into sc values(?,?)",studentID, courseID);
    }
    /**删除一个角色的选课记录*/
    public void delete(String studentName) {
        this.executeSql("delete from sc where stuID in(select stuID from student where stuName = ?)", studentName);
    }
    /**
     * 查询一个角色的选课记录
     */
    public List<Course> getSelectedCourse(String studentName) {
        List<Course> list=new ArrayList<>();
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            ps=connection.prepareStatement("select * from course where courseID in (select courseID from sc where stuID in (select stuID from student where stuName = '" + studentName + "'))");
            rs=ps.executeQuery();
            while (rs.next()) {
                Course c = new Course();
                c.setCourseID(rs.getInt("courseID"));
                c.setCourseName(rs.getString("courseName"));
                c.setCourseCredit(rs.getDouble("courseCredit"));
                c.setMagicCost(rs.getDouble("magicCost"));
                c.setIsCompulsory(rs.getString("isCompulsory"));
                list.add(c);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return list;
    }

    /**
     * 检查sc表中是否有这个学生选这门课的记录
     * @param studentID 学生ID
     * @param courseID 课程ID
     * @return 布尔值
     */
    public boolean checkStudentCourse(Integer studentID, Integer courseID) {
        boolean check = false;
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            ps=connection.prepareStatement("select * from sc where courseID=? and stuID=?",courseID, studentID);
            ps.setString(1, courseID.toString());
            ps.setString(2, studentID.toString());
            rs=ps.executeQuery();
            if(rs.next()) check = true;
            return check;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return false;
    }
}
