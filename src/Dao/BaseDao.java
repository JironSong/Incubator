package Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 基础的Dao类
 * @author 宋子龙
 */
public class BaseDao {
    private static final String url = "jdbc:mysql://localhost:3306/mahoshojo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CST";
    private static final String driver = "com.mysql.cj.jdbc.Driver";
    private static final String user = "fmACG";
    private static final String password = "114514";
    /**
     * 封装获得连接的方法
     * @return
     */
    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(driver);//加载驱动
            connection= DriverManager.getConnection(url,user,password);//获得连接
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * 封装对继承AutoCloseable接口类的关闭操作
     * @param object
     */
    public void close(AutoCloseable object) {
        if(object!=null) {
            try {
                object.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 执行sql语句，可变参数为sql语句中需要的参数
     * @param sql 语句
     * @param objs 语句中需要的参数
     */
    public void executeSql(String sql,Object...objs) {

        Connection conn=this.getConnection();
        PreparedStatement ps=null;
        try {
            ps=conn.prepareStatement(sql);
            //循环为每一个变量设置参数
            for(int i=0;i<objs.length;i++) {
                ps.setObject(i+1, objs[i]);
            }
            //执行sql
            ps.execute();
            //System.out.println("执行   "+sql+"  成功");
        }catch(Exception e) {
            e.printStackTrace();
        }finally {
            this.close(ps);
            this.close(conn);
        }
    }

    /**
     * 测试数据库连接
     */
    public static void main(String[] args) {
        BaseDao baseDao = new BaseDao();
        baseDao.getConnection();
    }
}
