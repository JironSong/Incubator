package Dao;

import Entity.Course;
import View.ShowCourses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * CourseDao，具有查询插入删除更新的功能
 * @author 宋子龙
 */
public class CourseDao extends BaseDao{
    private static CourseDao instance;
    private CourseDao() {
    }
    public static CourseDao getInstance() {
        if (instance == null) instance = new CourseDao();
        return instance;
    }
    /**插入记录*/
    public void save(Course course) {
        this.executeSql("insert into course values(null,?,?,?,?)", course.getCourseName(),course.getCourseCredit(),course.getMagicCost(),course.getIsCompulsory());
    }
    /**更新记录*/
    public void update(Course course) {
        this.executeSql("update course set magicCost=?,courseCredit=? where courseName=?", course.getMagicCost(),course.getCourseCredit(),course.getIsCompulsory(),course.getCourseName());
    }

    /**删除记录*/
    public void delete(Integer courseID) {
        this.executeSql("Delete from course where courseID=?", courseID);
    }
    /**删除记录*/
    public void delete(String courseName) {
        this.executeSql("Delete from course where courseName=?", courseName);
    }
    /**查询所有记录*/
    public List<Course> getAllCourse() {
        List<Course> list=new ArrayList<>();
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            //查询出所有的记录，按照courseID排序
            ps=connection.prepareStatement("select courseID,courseName,courseCredit,magicCost,isCompulsory from course order by courseID");
            rs=ps.executeQuery();
            while(rs.next()) {
                Course b=new Course();//创建一个vo对象存储记录的信息
                b.setCourseID(rs.getInt("courseID"));
                b.setCourseName(rs.getString("courseName"));
                b.setCourseCredit(rs.getDouble("courseCredit"));
                b.setMagicCost(rs.getDouble("magicCost"));
                b.setIsCompulsory(rs.getString("isCompulsory"));
                list.add(b);//每条记录添加到list中
            }
            return list;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return list;
    }

    /**查询所有课程的名字，按课程ID排序*/
    public List<String> getAllCourseName() {
        List<String> list=new ArrayList<>();
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            ps=connection.prepareStatement("select courseName from course order by courseID");
            rs=ps.executeQuery();
            while(rs.next()) {
                String b = rs.getString("courseName");
                list.add(b);
            }
            return list;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return list;
    }

    /**
     * 根据课程名查询单个课程的属性
     * @return 长度为3的String数组，第0个是学分，第1个是魔力消耗值，第2个是是否为必修课的“是”或“否”
     */
    public List<String> getAttributesByCourseName(String courseName) {
        List<String> list=new ArrayList<>();
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            ps=connection.prepareStatement("select courseCredit,magicCost,isCompulsory from course where courseName = '"+ courseName + "'");
            rs=ps.executeQuery();
            if (rs.next()) {
                list.add("" + rs.getDouble("courseCredit"));
                list.add("" + rs.getDouble("magicCost"));
                list.add(rs.getString("isCompulsory"));
            }
            return list;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return list;
    }

    /**
     * 根据课程名查询它的ID（主键）
     * @param courseName 课程名
     * @return 课程ID
     */
    public int getCourseIDByName(String courseName) {
        int courseID = -1;
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            ps=connection.prepareStatement("select courseID from course where courseName = '"+ courseName + "'");
            rs=ps.executeQuery();
            if (rs.next()) {
                courseID = rs.getInt("courseID");
            }
            return courseID;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return courseID;
    }

    /**
     * 根据课程名查询单个课程的魔力消耗值
     * @return 魔力消耗值
     */
    public double getMagicCostByName(String courseName) {
        double magicCost = 0.0;
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            ps=connection.prepareStatement("select magicCost from course where courseName = '"+ courseName + "'");
            rs=ps.executeQuery();
            if (rs.next()) {
                magicCost = rs.getInt("magicCost");
            }
            return magicCost;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return magicCost;
    }

}
