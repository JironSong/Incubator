package Dao;
import Entity.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * StudentDao，具有查询插入删除更新的功能
 * @author 宋子龙
 */
public class StudentDao extends BaseDao {
    private static StudentDao instance;
    private StudentDao() {
    }
    public static StudentDao getInstance() {
        if (instance == null) instance = new StudentDao();
        return instance;
    }
    /**插入记录*/
    public void save(Student student) {
        this.executeSql("insert into Student values(null,?,?,?,?,?,?)", student.getStuName(),student.getMagicRate(),student.getLuck(),student.getIsLiving(),student.getMagic(),student.getIsGraduated());
    }

    /**更新记录*/
    public void update(Student student) {
        this.executeSql("update Student set isLiving=?,magic=?,isGraduated=? where stuName=?", student.getIsLiving(),student.getMagic(),student.getIsGraduated(),student.getStuName());
    }

    /**清空表*/
    public void delete() {
        this.executeSql("Delete from Student");
    }

    /**查询所有记录*/
    public List<Student> getAllStudent() {
        List<Student> list=new ArrayList<>();
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            //查询出所有的记录，按照stuID排序
            ps=connection.prepareStatement("select stuID,stuName,magic,magicRate,luck,isLiving,isGraduated from Student order by stuID");
            rs=ps.executeQuery();
            while(rs.next()) {
                Student b=new Student();//创建一个vo对象存储记录的信息
                b.setStuID(rs.getInt("stuID"));
                b.setStuName(rs.getString("StuName"));
                b.setMagic(rs.getDouble("magic"));
                b.setMagicRate(rs.getDouble("magicRate"));
                b.setLuck(rs.getDouble("luck"));
                b.setIsLiving(rs.getInt("isLiving"));
                list.add(b);//每条记录添加到list中
            }
            return list;
        }catch(Exception e) {
            e.printStackTrace();
        }finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return list;
    }

    /**根据id查询单条记录*/
    public Student getByStuID(Integer StuID) {
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        Student b=new Student();
        try {
            ps=connection.prepareStatement("select stuID,stuName,magic,magicRate,luck,isLiving,isGraduated from Student where stuID = ?", StuID);
            rs=ps.executeQuery();
            if(rs.next()) {
                b.setStuID(rs.getInt("stuID"));
                b.setStuName(rs.getString("StuName"));
                b.setMagic(rs.getDouble("magic"));
                b.setMagicRate(rs.getDouble("magicRate"));
                b.setLuck(rs.getDouble("luck"));
                b.setIsLiving(rs.getInt("isLiving"));
            }
            return b;
        }catch(Exception e) {
            e.printStackTrace();
        }finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return b;
    }

    /**
     * 根据学生名字查询学生ID（主键）
     * @param studentName 学生名字
     * @return 学生ID
     */
    public int getStudentIDByName(String studentName) {
        int studentID = -1;
        Connection connection=this.getConnection();
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            ps=connection.prepareStatement("select stuID from student where stuName = '"+ studentName + "'");
            rs=ps.executeQuery();
            if (rs.next()) {
                studentID = rs.getInt("stuID");
            }
            return studentID;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.close(rs);
            this.close(ps);
            this.close(connection);
        }
        return studentID;
    }

}
