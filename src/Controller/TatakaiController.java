package Controller;

import Dao.CourseDao;
import Dao.ScDao;
import Dao.StudentDao;
import Entity.Student;
import Util.GlobalUtil;
import View.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.stage.Stage;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * 战斗的控制类
 * @author 宋子龙，朱穆清
 */
public class TatakaiController implements Initializable {
    @FXML
    private Label creditLabel;
    @FXML
    private Label magicLabel;
    @FXML
    private Label compulsoryLabel;
    @FXML
    private Label GSLabel;
    @FXML
    private Label currentMagicLabel;
    @FXML
    private Button decideButton;
    @FXML
    private ComboBox<String> selectCourseComboBox;
    @FXML
    private Button confirmCourseButton;
    @FXML
    private RadioButton JRadioButton1;
    @FXML
    private RadioButton JRadioButton2;
    @FXML
    private RadioButton JRadioButton3;
    @FXML
    private RadioButton JRadioButton4;
    @FXML
    private RadioButton MRadioButton1;
    @FXML
    private RadioButton MRadioButton2;
    @FXML
    private RadioButton MRadioButton3;
    @FXML
    private RadioButton MRadioButton4;
    @FXML
    private Button GSButton;

    // 表格数据
    private final ObservableList<String> tableData = FXCollections.observableArrayList();
    private final StudentDao studentDao = StudentDao.getInstance();
    private final ScDao scDao = ScDao.getInstance();
    private final CourseDao courseDao = CourseDao.getInstance();

    /**现在的角色*/
    public static Student student;

    /**
     * 战斗控制类初始化，将当前角色没选过的课放进comboBox
     * 先把确定课程和做出决定按钮都设为不可见
     * 选定课程后确定课程按钮才可见(lambda那句）
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(student==null) student = GlobalUtil.getInstance().findCurrentStudent();
        //初始化课程选项
        tableData.clear();
        for(String course: courseDao.getAllCourseName()) {
            if( !(scDao.checkStudentCourse(studentDao.getStudentIDByName(student.getStuName()), courseDao.getCourseIDByName(course))) )
                tableData.add(course);
        }
        selectCourseComboBox.getItems().removeAll(selectCourseComboBox.getItems());
        selectCourseComboBox.getItems().addAll(tableData);

        decideButton.setDisable(true);
        confirmCourseButton.setDisable(true);
        GSButton.setDisable(true);
        selectCourseComboBox.getSelectionModel().selectedItemProperty().addListener((observableValue, s, t1) -> confirmCourseButton.setDisable(false));
        if(GlobalUtil.getInstance().getGriefSeed()>0) GSButton.setDisable(false);
        GSLabel.setText(GlobalUtil.getInstance().getGriefSeed() + "");
        currentMagicLabel.setText(student.getMagic() + "");
    }

    /**
     * 使用griefSeed的确认按钮，按下后消耗一个griefSeed给当前角色补充魔力，并跳过选课环节到下一个角色
     */
    @FXML
    private void use_griefSeed(ActionEvent event) throws Exception {
        GlobalUtil util = GlobalUtil.getInstance();
        util.setGriefSeed(util.getGriefSeed()-1);
        student.setMagic(student.getMagic()+10.0);
        studentDao.update(student); //更新数据库
        student = GlobalUtil.getInstance().findNextStudent();
        Stage stage = (Stage) decideButton.getScene().getWindow();
        switch (student.getStuName()) { // 在当前的stage显示新的页面
            case "Homura" -> TatakaiHomura.getInstance().run(stage);
            case "Madoka" -> TatakaiMadoka.getInstance().run(stage);
            case "Kyouko" -> TatakaiKyouko.getInstance().run(stage);
            case "Sayaka" -> TatakaiSayaka.getInstance().run(stage);
            case "Mami" -> TatakaiMami.getInstance().run(stage);
        }
    }
    /**
     * 监听选择课程的确定按钮
     */
    @FXML
    private void decide_course(ActionEvent event) {
        String courseName = selectCourseComboBox.getValue();
        List<String> courseInfo = courseDao.getAttributesByCourseName(courseName);
        creditLabel.setText(courseInfo.get(0));
        magicLabel.setText(courseInfo.get(1));
        compulsoryLabel.setText(courseInfo.get(2));
        decideButton.setDisable(false); // 此时才显示出提交按钮
    }

    /**
     * 做出选课决定的确认按钮
     * 按下按钮后扣除魔力值，将选课信息添加到数据库，概率计算是否获得griefSeed，更新学生的魔力值并判断是否死亡、是否完成培养方案，然后跳转到下一个页面
     */
    @FXML
    private void make_decision(ActionEvent event) throws Exception {
        String courseName = selectCourseComboBox.getValue();
        Stage stage = (Stage) decideButton.getScene().getWindow();
        // 计算卷和摸的数值
        double offset = student.getLuck();
        double magicCost = courseDao.getMagicCostByName(courseName);
        double additional = 0.0;
        if(JRadioButton1.isSelected()) {
            offset += 0.1;
            additional += 0.1*magicCost*student.getMagicRate();
        }
        if(JRadioButton2.isSelected()) {
            offset += 0.2;
            additional += 0.2*magicCost*student.getMagicRate();
        }
        if(JRadioButton3.isSelected()) {
            offset += 0.3;
            additional += 0.3*magicCost*student.getMagicRate();
        }
        if(JRadioButton4.isSelected()) {
            offset += 0.4;
            additional += 0.4*magicCost*student.getMagicRate();
        }
        double ran = Math.random()/Math.nextDown(1.0) + offset;
        if(ran>=1.7) {
            GlobalUtil util = GlobalUtil.getInstance();
            util.setGriefSeed(util.getGriefSeed()+1);
        }
        student.setMagic( student.getMagic() - (magicCost*student.getMagicRate() + additional) );
        // 根据本轮结果更新学生状态
        scDao.save(studentDao.getStudentIDByName(student.getStuName()), courseDao.getCourseIDByName(courseName));
        if(student.getMagic()<=0.0)
            student.setIsLiving(0); //死亡
        else if(GlobalUtil.getInstance().checkGraduate(student))
            student.setIsGraduated(1); //毕业
        //更新数据库
        studentDao.update(student);
        //切换角色
        student = GlobalUtil.getInstance().findNextStudent();
        if(student==null) { //为空则跳转入结算页面
            Graduate.getInstance().run(stage);
        }
        else{
            switch (student.getStuName()) { // 在当前的stage显示新的页面
                case "Homura" -> TatakaiHomura.getInstance().run(stage);
                case "Madoka" -> TatakaiMadoka.getInstance().run(stage);
                case "Kyouko" -> TatakaiKyouko.getInstance().run(stage);
                case "Sayaka" -> TatakaiSayaka.getInstance().run(stage);
                case "Mami" -> TatakaiMami.getInstance().run(stage);
            }
        }
    }
}
