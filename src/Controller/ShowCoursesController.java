package Controller;

import Dao.CourseDao;
import Entity.Course;
import View.AddCourse;
import View.OpenWindow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;


import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import org.w3c.dom.events.Event;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * 显示培养方案的控制类
 * @author 朱穆清
 */
public class ShowCoursesController implements Initializable {

    // 注册页面组件
    @FXML
    private TableView<Course> courseTable;
    @FXML
    private TableColumn<Course, String> nameCol;
    @FXML
    private TableColumn<Course, String> compulsoryCol;
    @FXML
    private TableColumn<Course, Double> creditCol;
    @FXML
    private TableColumn<Course, Double> magicCol;
    @FXML
    private Button addButton;
    @FXML
    private Button backButton;
    @FXML
    private Button delButton;

    // 表格数据
    private final ObservableList<Course> tableData = FXCollections.observableArrayList();
    private final CourseDao courseDao = CourseDao.getInstance();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // 反射取值
        nameCol.setCellValueFactory(new PropertyValueFactory<>("courseName"));
        compulsoryCol.setCellValueFactory(new PropertyValueFactory<>("isCompulsory"));
        creditCol.setCellValueFactory(new PropertyValueFactory<>("courseCredit"));
        magicCol.setCellValueFactory(new PropertyValueFactory<>("magicCost"));
        // 表格的初始化
        tableData.addAll(courseDao.getAllCourse());
        courseTable.setItems(tableData);
    }
    @FXML
    public void setAddButton(ActionEvent event) throws Exception {
        Stage stage = (Stage) addButton.getScene().getWindow();  // 在controller中通过组件获取窗口对象
        AddCourse.getInstance().run(stage);  // 跳转到课程添加页面
    }
    @FXML
    public void setBackButton(ActionEvent event) throws Exception {
        Stage stage = (Stage) backButton.getScene().getWindow();
        OpenWindow.getInstance().run(stage);  // 跳转回主界面
    }
    @FXML
    public void setDelButton(ActionEvent event) throws Exception {
        Course courseSelected = courseTable.getSelectionModel().getSelectedItem();  // 获得表格中当前选择的对象
        CourseDao.getInstance().delete(courseSelected.getCourseID());  // 调用数据库删除这条记录
        courseTable.getItems().remove(courseSelected);  // 从表格中删除这条记录
    }


}
