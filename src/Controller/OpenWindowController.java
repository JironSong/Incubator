package Controller;

import Dao.CourseDao;
import Dao.ScDao;
import Dao.StudentDao;
import Util.GlobalUtil;
import View.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * 初始页面的控制类
 * @author 宋子龙
 */
public class OpenWindowController implements Initializable {
    @FXML
    private Button game_exit;
    @FXML
    private Button game_start;
    @FXML
    private Button game_setting;
    // 表格数据
    private final CourseDao courseDao = CourseDao.getInstance();
    private final StudentDao studentDao = StudentDao.getInstance();
    private final ScDao scDao = ScDao.getInstance();

    /**
     * 如果培养方案为空，则战斗按钮不可见
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        game_start.setDisable(true);
        if(!courseDao.getAllCourse().isEmpty()) game_start.setDisable(false);
        // 清空sc表(放在这里，随时关闭后重新打开进入战斗，不会有异常)
        scDao.delete();
    }
    /**
     * 进入开始游戏界面
     * 优化结构by朱穆清（2021/6/10）：
     * 这种写法要求在fxml中对应的事件源登记响应方法名（onAction="#setGame_start）
     * 就可以不写initialize方法也能直接响应（不用点两次）了。
     */
    @FXML
    private void setGame_start(ActionEvent event) throws Exception {
        GlobalUtil.getInstance().init();
        Stage stage = (Stage) game_start.getScene().getWindow();
        TatakaiHomura.getInstance().run(stage);
    }

    /**
     * 进入QB设置培养方案的界面
     */
    @FXML
    private void setGame_setting(ActionEvent event) throws Exception {
        Stage stage = (Stage) game_setting.getScene().getWindow();  // 在controller中通过组件获取窗口对象
        ShowCourses.getInstance().run(stage);  // 在当前的stage显示新的页面
    }

    /**
     * 退出游戏
     * 原来的风格的写法先通过注释保留
     */
    @FXML
    private void setGame_exit(ActionEvent event) {
        System.exit(0);
    }
}
