package Controller;

import Dao.CourseDao;
import Entity.Course;
import View.ShowCourses;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.*;

/**
 * 添加课程的控制类
 * @author 宋子龙，朱穆清
 */
public class AddCourseController implements Initializable {

    // 注册FXML组件
    @FXML
    private Button submit;
    @FXML
    private Button cancel;
    @FXML
    private TextField name;
    @FXML
    private ComboBox<String> magicComboBox;
    @FXML
    private ComboBox<String> creditComboBox;
    @FXML
    private RadioButton isCompulsory;
    @FXML
    private RadioButton notCompulsory;
    @FXML
    private Label nameNullLabel;

    private final CourseDao courseDao = CourseDao.getInstance();


    /**
     * 下选框（ComboBox）只能用这种方式初始化！没法在scenebuilder里直接设置条目！！！
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // 初始化学分选项
        creditComboBox.getItems().removeAll(creditComboBox.getItems());
        creditComboBox.getItems().addAll("0.5", "1.0", "2.0", "3.0", "4.0", "6.0");
        creditComboBox.getSelectionModel().select("2.0");
        // 初始化魔力值选项
        magicComboBox.getItems().removeAll(creditComboBox.getItems());
        magicComboBox.getItems().addAll("0.5", "1.0", "2.0", "3.0", "4.0", "6.0");
        magicComboBox.getSelectionModel().select("2.0");
    }

    /**
     * 监听取消事件
     */
    @FXML
    private void setCancel(ActionEvent event) throws Exception {
        Stage stage = (Stage) cancel.getScene().getWindow();  // 在controller中通过组件获取窗口对象的方法
        ShowCourses.getInstance().run(stage);
    }

    /**
     * 监听提交事件
     */
    @FXML
    private void setSubmit(ActionEvent event) throws Exception {
        if(name.getText().trim().equals("")) {
            nameNullLabel.setVisible(true);
            name.clear();
            return;
        }
        nameNullLabel.setVisible(false);
        String courseName = name.getText();  // 获得课程名
        double courseCredit = Double.parseDouble(creditComboBox.getValue());  // 获得课程学分
        double courseMagic = Double.parseDouble(magicComboBox.getValue());  // 获得课程学分
        String courseCompulsory;  // 是否必修
        if(isCompulsory.isSelected())
            courseCompulsory = "是";
        else courseCompulsory = "否";
        System.out.printf("课程名：%s%n学分：%f%n魔力值：%f%n是必修课：%b%n", courseName, courseCredit, courseMagic, courseCompulsory);  // 仅为测试

        // 添加至数据库
        Course course = new Course(courseName, courseCompulsory, courseCredit, courseMagic);
        courseDao.save(course);

        Stage stage = (Stage) cancel.getScene().getWindow();  // 在controller中通过组件获取窗口对象的方法
        ShowCourses.getInstance().run(stage);
    }
}