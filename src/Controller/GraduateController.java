package Controller;

import Entity.Student;
import Util.GlobalUtil;
import View.OpenWindow;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;


import javafx.event.ActionEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class GraduateController implements Initializable {
    @FXML
    private Button game_exit;
    @FXML
    private Label madokaLabel;
    @FXML
    private Label homuraLabel;
    @FXML
    private Label mamiLabel;
    @FXML
    private Label sayakaLabel;
    @FXML
    private Label kyoukoLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ArrayList<Student> finished = GlobalUtil.getInstance().getFinished();
        for(Student student : finished) {
            switch (student.getStuName()) {
                case "Madoka" -> setText(madokaLabel,student);
                case "Homura" -> setText(homuraLabel,student);
                case "Kyouko" -> setText(kyoukoLabel,student);
                case "Sayaka" -> setText(sayakaLabel,student);
                case "Mami" -> setText(mamiLabel,student);
            }
        }
    }

    /**
     * 设置角色的结局状态
     * @param label 状态标签
     * @param student 角色
     */
    public void setText(Label label, Student student) {
        if(student.getIsGraduated()==1) label.setText("卒業");
        else if(student.getIsLiving()==0) label.setText("「玉砕」");
    }

    /**
     * 游戏结束
     * @param event
     */
    @FXML
    private void setGame_exit(ActionEvent event) throws Exception {
        Stage stage = (Stage) game_exit.getScene().getWindow();
        OpenWindow.getInstance().run(stage);
    }
}
