package Entity;
/**
 * 魔法少女的类
 * @author 宋子龙
 */
public class Student {
    private int stuID;
    private String stuName;
    /**初始的魔力值*/
    private double magic;
    /**扣除魔力值的比率，基础值是1*/
    private double magicRate = 1.0;
    /**获得悲叹之种的运气（概率偏移量）*/
    private double luck;
    /**是否还活着*/
    private int isLiving = 1; //新添加的肯定是活的
    /**是否已经毕业（完成培养方案）*/
    private int isGraduated = 0; //新添加的肯定是未毕业

    public Student() { }

    public String getStuName() { return stuName; }
    public void setStuName(String stuName) { this.stuName = stuName; }
    public double getMagicRate() { return magicRate; }
    public void setMagicRate(double magicRate) { this.magicRate = magicRate; }
    public double getLuck() { return luck; }
    public void setLuck(double luck) { this.luck = luck; }
    public int getIsLiving() { return isLiving; }
    public void setIsLiving(int isLiving) { this.isLiving = isLiving; }
    public double getMagic() { return magic; }
    public void setMagic(double magic) { this.magic = magic; }
    public int getStuID() { return stuID; }
    public void setStuID(int stuID) { this.stuID = stuID; }
    public int getIsGraduated() { return isGraduated; }
    public void setIsGraduated(int isGraduated) { this.isGraduated = isGraduated; }

    /**
     * 创建一个新的魔法少女
     * @param stuName 名字
     * @param magic 初始魔力值
     * @param magicRate 扣除魔力值的比率，基础值是1
     * @param luck 获得悲叹之种的运气（概率偏移量）
     */
    public Student(String stuName, double magic, double magicRate, double luck) {
        this.stuName = stuName;
        this.magic = magic;
        this.magicRate = magicRate;
        this.luck = luck;
    }

    @Override
    public String toString() {
        return "Student{" +
                "stuID=" + stuID +
                ", stuName='" + stuName + '\'' +
                ", magic=" + magic +
                ", magicRate=" + magicRate +
                ", luck=" + luck +
                ", isLiving=" + isLiving +
                ", isGraduated=" + isGraduated +
                '}';
    }
}
