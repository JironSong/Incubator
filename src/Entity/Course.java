package Entity;

import javafx.beans.property.*;

/**
 * 培养方案中的课程类
 * @author 宋子龙
 */
public class Course {
    private final IntegerProperty courseID = new SimpleIntegerProperty();
    private StringProperty courseName = new SimpleStringProperty();
    /**是否为必修课*/
    private StringProperty isCompulsory = new SimpleStringProperty();
    /**每门课的学分*/
    private DoubleProperty courseCredit = new SimpleDoubleProperty();
    /**每门课有一个基础的魔力消耗值，且不一定与学分成正比*/
    private DoubleProperty magicCost = new SimpleDoubleProperty();

    public int getCourseID() { return courseID.get(); }
    public IntegerProperty courseIDProperty() { return courseID; }
    public void setCourseID(int courseID) { this.courseID.set(courseID); }

    public String getCourseName() { return courseName.get(); }
    public StringProperty courseNameProperty() { return courseName; }
    public void setCourseName(String courseName) { this.courseName.set(courseName); }

    public String getIsCompulsory() { return isCompulsory.get(); }
    public StringProperty isCompulsoryProperty() { return isCompulsory; }
    public void setIsCompulsory(String isCompulsory) { this.isCompulsory.set(isCompulsory); }

    public double getCourseCredit() { return courseCredit.get(); }
    public DoubleProperty courseCreditProperty() { return courseCredit; }
    public void setCourseCredit(double courseCredit) { this.courseCredit.set(courseCredit); }

    public double getMagicCost() { return magicCost.get(); }
    public DoubleProperty magicCostProperty() { return magicCost; }
    public void setMagicCost(double magicCost) { this.magicCost.set(magicCost); }


    public Course() { }

    /**
     * QB向课程表中添加一门新的课程
     * @param courseName 课程名字
     * @param isCompulsory 是否为必修课
     * @param courseCredit 学分
     * @param magicCost 魔力值消耗，不一定与学分成正比
     */
    public Course(String courseName, String isCompulsory, Double courseCredit, Double magicCost) {
        this.courseName = new SimpleStringProperty(courseName);
        this.isCompulsory = new SimpleStringProperty(isCompulsory);
        this.courseCredit = new SimpleDoubleProperty(courseCredit);
        this.magicCost = new SimpleDoubleProperty(magicCost);
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseID=" + courseID +
                ", courseName=" + courseName +
                ", isCompulsory=" + isCompulsory +
                ", courseCredit=" + courseCredit +
                ", magicCost=" + magicCost +
                '}';
    }
}
