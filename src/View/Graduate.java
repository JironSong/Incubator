package View;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * 结算（毕业）窗口
 * @author 谢吉力
 */
public class Graduate {
    private static Graduate instance;
    private Graduate() {}
    public static Graduate getInstance() {
        if (instance == null) instance = new Graduate();
        return instance;
    }
    /**
     * 结算窗口，展示所有角色的存活情况
     * @param primaryStage 结算界面
     * @throws Exception 任何意外
     */
    public void run(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Fxml/Graduate.fxml"));
        primaryStage.setTitle("结算界面");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
