package View;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * 程序的初始窗口
 * @author 朱穆清
 */
public class OpenWindow{
    private static OpenWindow instance;
    private OpenWindow() {
    }
    public static OpenWindow getInstance() {
        if (instance == null) instance = new OpenWindow();
        return instance;
    }
    /**
     * 打开程序的初始窗口
     * @param primaryStage 初始界面
     * @throws Exception 任何意外
     */
    public void run(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Fxml/OpenWindow.fxml"));
        primaryStage.setTitle("魔法少女选课管理系统");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
