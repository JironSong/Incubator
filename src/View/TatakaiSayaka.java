package View;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 * @author 宋子龙
 * 所有战斗页面都是单例模式
 */
public class TatakaiSayaka {
    private static TatakaiSayaka instance;
    private TatakaiSayaka() {
    }
    public static TatakaiSayaka getInstance() {
        if (instance == null) instance = new TatakaiSayaka();
        return instance;
    }
    /**
     * 战斗窗口，展示当前的战斗角色，即美树沙耶加
     * @param primaryStage 美树沙耶加战斗界面
     * @throws Exception 任何意外
     */
    public void run(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Fxml/TatakaiSayaka.fxml"));
        primaryStage.setTitle("战斗中");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
