package View;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 * @author 宋子龙
 * 所有战斗页面都是单例模式
 */
public class TatakaiMami {
    private static TatakaiMami instance;
    private TatakaiMami() {
    }
    public static TatakaiMami getInstance() {
        if (instance == null) instance = new TatakaiMami();
        return instance;
    }
    /**
     * 战斗窗口，展示当前的战斗角色，即巴麻美
     * @param primaryStage 巴麻美战斗界面
     * @throws Exception 任何意外
     */
    public void run(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Fxml/TatakaiMami.fxml"));
        primaryStage.setTitle("战斗中");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
