package View;

import Controller.AddCourseController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * 添加课程窗口
 * @author 朱穆清
 */
public class AddCourse {
    private static AddCourse instance;
    private AddCourse() {
    }
    public static AddCourse getInstance() {
        if (instance == null) instance = new AddCourse();
        return instance;
    }
    /**
     * 打开添加课程的窗口（单例模式）
     * @param primaryStage 添加课程界面
     * @throws Exception 任何意外
     */
    public void run(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Fxml/AddCourse.fxml"));
        primaryStage.setTitle("魔法少女培养方案添加");
        primaryStage.setScene(new Scene(root));  // 把场景载入窗口
        primaryStage.show();  // 显示窗口
    }
}
