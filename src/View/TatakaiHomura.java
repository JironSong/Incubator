package View;


import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author 宋子龙
 * 所有战斗页面都是单例模式
 */
public class TatakaiHomura {
    private static TatakaiHomura instance;
    private TatakaiHomura() {
    }
    public static TatakaiHomura getInstance() {
        if (instance == null) instance = new TatakaiHomura();
        return instance;
    }
    /**
     * 战斗窗口，展示当前的战斗角色，即晓美焰
     * @param primaryStage 晓美焰战斗界面
     * @throws Exception 任何意外
     */
    public void run(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Fxml/TatakaiHomura.fxml"));
        primaryStage.setTitle("战斗中");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
