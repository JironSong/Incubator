package View;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 * @author 宋子龙
 * 所有战斗页面都是单例模式
 */
public class TatakaiKyouko {
    private static TatakaiKyouko instance;
    private TatakaiKyouko() {
    }
    public static TatakaiKyouko getInstance() {
        if (instance == null) instance = new TatakaiKyouko();
        return instance;
    }
    /**
     * 战斗窗口，展示当前的战斗角色，即佐仓杏子
     * @param primaryStage 佐仓杏子战斗界面
     * @throws Exception 任何意外
     */
    public void run(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Fxml/TatakaiKyouko.fxml"));
        primaryStage.setTitle("战斗中");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
