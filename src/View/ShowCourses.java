package View;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 * 培养方案编辑窗口，展示当前的培养方案，即显示所有课程
 * @author 朱穆清
 */
public class ShowCourses {

    private static ShowCourses instance;
    private ShowCourses() {
    }
    public static ShowCourses getInstance() {
        if (instance == null) instance = new ShowCourses();
        return instance;
    }

    /**
     * 打开显示培养方案的窗口
     * @param primaryStage 初始界面
     * @throws Exception 任何意外
     */
    public void run(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Fxml/ShowCourses.fxml"));
        primaryStage.setTitle("魔法少女培养方案列表");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
